package sheridan.adeel.student;

import java.util.Scanner;

/**
 * This class is a simple example of creating arrays of objects
 *
 * @author Paul Bonenfant
 */
public class StudentList {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        //Declaring string array called student
        String[] student = new String[args.length];
        String[] program = {"Java", "Python", "Java Script"};
        //using for loop to assign args value to string array called student
        for(int i = 0; i<args.length;i++)
        {
            student[i] = args[i];
        }
        String program;
        
        //Creating an object of student and passing parameters for name and id
        //Displaying results
        Student s = new Student(student[0],student[1]);
        String format = "The student's name is %s and student's ID is %s \n";
        
        System.out.printf(format, s.getName(), s.getStudentId());
        //Creating an object of student again and passing parameters 
        //for name and id
        //Displaying results
        s = new Student(student[2],student[3]);
        format = "The student's name is %s and student's ID is %s \n";
        
        System.out.printf(format, s.getName(), s.getStudentId());
    }
}

