package sheridan.adeel.student;

/**
 * This class represents students in our application
 *
 * @author Paul Bonenfant
 */
public class Student {
    
    private String name, id;
    
    private String program;

    public Student(String name, String studentId, String program) {
        this.name = name;
        this.id = studentId;
        this.program = program;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getStudentId()
    {
        return id;
    }
    
    public String getProgram()
    {
        return this.program;
    }
    public void setProgram(String program)
    {
        this.program = program;
    }
}